package com.m0rb1u5.pdm_lab4_2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class Segunda extends AppCompatActivity {
   private static final int SELECT_PHOTO = 100;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_segunda);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_segunda, menu);
      return true;
   }

   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      Uri selectedImage;
      switch (requestCode) {
         case SELECT_PHOTO:
            if (resultCode == Activity.RESULT_OK) {
               selectedImage = data.getData();
               InputStream inputStream = null;
               try {
                  inputStream = getContentResolver().openInputStream(selectedImage);
               }
               catch (FileNotFoundException error) {
                  error.printStackTrace();
                  Log.e("Error", error.toString());
               }
               Bitmap bmp = BitmapFactory.decodeStream(inputStream);
               ImageView imageView = (ImageView) findViewById(R.id.imageView);
               imageView.setImageBitmap(bmp);
            }
         break;
      }
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   public void cargarimagen(View View) {
      Intent intent = new Intent();
      intent.setType("image/*");
      intent.setAction(Intent.ACTION_GET_CONTENT);
      startActivityForResult(Intent.createChooser(
            intent,"Seleccione una imagen"), SELECT_PHOTO);
   }

   public void volver(View view){
      Log.d("App", "Adios con dios");
      finish();
   }
}
